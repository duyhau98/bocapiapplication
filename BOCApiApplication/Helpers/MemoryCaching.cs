﻿
using BOCApiApplication.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BOCApiApplication.Helpers
{
    public static class MemoryCaching
    {
        
        public static IMemoryCache _cache;
        public static DatabaseContext _context;

        public static void CacheSymbols()
        {
            _cache.Set("Symbols", _context.Symbols.ToArray());
        }

        public static object getSymbols()
        {
            return _cache.Get("Symbols");
        }
    }
}
