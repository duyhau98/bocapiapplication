﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using BOCApiApplication.Common;
using System.Threading;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuyselltransactionsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public BuyselltransactionsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Buyselltransactions
        [HttpGet]
        public IEnumerable<Buyselltransactions> GetBuyselltransactions()
        {
            return _context.Buyselltransactions;
        }

        // GET: api/Buyselltransactions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBuyselltransactions([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var buyselltransactions = await _context.Buyselltransactions.FindAsync(id);

            if (buyselltransactions == null)
            {
                return NotFound();
            }

            return Ok(buyselltransactions);
        }

        [HttpGet("/api/symbol-buy-amount-{symbolId}-{accountId}-{userId}")]
        public IActionResult GetSymbolBuyAmmount(int? symbolId, int? accountId, int? userId)
        {
            if (symbolId == null||accountId==null||userId==null)
            {
                return BadRequest();
            }

            var parameters = new Dictionary<string, object>();
            parameters.Add("idSymbol", symbolId);
            parameters.Add("idAccount", accountId);
            var symbolAmount = Utilities.ExecuteStoreProcedure("GET_SYMBOL_AMOUNT_REMAINING", parameters);

            return Ok(symbolAmount);
        }

        [HttpGet("/api/symbol-buy-in-account")]
        public IActionResult GetSymbolInAccount()
        {

            var symbolAmount = Utilities.GetViewFromDB("viewsymbolinaccount");

            return Ok(symbolAmount);
        }

        // PUT: api/Buyselltransactions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBuyselltransactions([FromRoute] int id, [FromBody] Buyselltransactions buyselltransactions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != buyselltransactions.Id)
            {
                return BadRequest();
            }

            _context.Entry(buyselltransactions).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BuyselltransactionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }



        // POST: api/Buyselltransactions
        [HttpPost("/api/add-buyselltransactions-buysell-{isSoldOut}")]
        public async Task<IActionResult> PostBuyselltransactions([FromRoute] int amount, [FromRoute] int isSoldOut,  [FromBody] Buyselltransactions buyselltransactions)
        {

            try
            {
                Buyselltransactions buyselltransactionsTmp = new Buyselltransactions();
                buyselltransactions.Date = DateTime.Now;
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if(buyselltransactions.Type==false)
                {
                    buyselltransactions.Volume = buyselltransactions.Volume * (-1);
                }
                if (isSoldOut == 1)
                {
                    buyselltransactions.Inend = false;
                    var buyTrackings = _context.Buyselltransactions.Where(x => x.Symbol_id == buyselltransactions.Symbol_id && x.Inend == true&&x.Account_id==buyselltransactions.Account_id).ToList();
                    buyTrackings.ForEach(x => x.Inend = false);
                    _context.SaveChanges();
                }   
                    
                _context.Buyselltransactions.Add(buyselltransactions);
                _context.SaveChanges();


                Trackinguser trackinguser = new Trackinguser(buyselltransactions.User_id, DateTime.Now, 1, "buyselltransactions", (int)buyselltransactions.Id);
                trackinguser.Attribute = "price";
                trackinguser.Oldvalue = "";
                trackinguser.Newvalue = buyselltransactions.Price.ToString();
                _context.Trackingusers.Add(trackinguser);

                trackinguser = new Trackinguser(buyselltransactions.User_id, DateTime.Now, 1, "buyselltransactions", (int)buyselltransactions.Id);
                trackinguser.Attribute = "type";
                trackinguser.Oldvalue = "";
                trackinguser.Newvalue = buyselltransactions.Type.ToString();
                _context.Trackingusers.Add(trackinguser);

                trackinguser = new Trackinguser(buyselltransactions.User_id, DateTime.Now, 1, "buyselltransactions", (int)buyselltransactions.Id);
                trackinguser.Attribute = "volume";
                trackinguser.Oldvalue = "";
                trackinguser.Newvalue = buyselltransactions.Volume.ToString();
                _context.Trackingusers.Add(trackinguser);

                trackinguser = new Trackinguser(buyselltransactions.User_id, DateTime.Now, 1, "buyselltransactions", (int)buyselltransactions.Id);
                trackinguser.Attribute = "thesis";
                trackinguser.Oldvalue = "";
                trackinguser.Newvalue = buyselltransactions.Thesis.ToString();
                _context.Trackingusers.Add(trackinguser);
                _context.SaveChanges();

                //_context.Buyselltransactions.Add(buyselltransactions);
                //await _context.SaveChangesAsync();
                //trackinguser = new Trackinguser(buyselltransactions.User_id, DateTime.Now, 1, "buyselltransactions", (int)buyselltransactions.Id);
                //trackinguser.Attribute = "price";
                //trackinguser.Oldvalue = "";
                //trackinguser.Newvalue = buyselltransactions.Price.ToString();
                //_context.Trackingusers.Add(trackinguser);

                //trackinguser = new Trackinguser(buyselltransactions.User_id, DateTime.Now, 1, "buyselltransactions", (int)buyselltransactions.Id);
                //trackinguser.Attribute = "type";
                //trackinguser.Oldvalue = "";
                //trackinguser.Newvalue = buyselltransactions.Type.ToString();
                //_context.Trackingusers.Add(trackinguser);

                //trackinguser = new Trackinguser(buyselltransactions.User_id, DateTime.Now, 1, "buyselltransactions", (int)buyselltransactions.Id);
                //trackinguser.Attribute = "volume";
                //trackinguser.Oldvalue = "";
                //trackinguser.Newvalue = buyselltransactions.Volume.ToString();
                //_context.Trackingusers.Add(trackinguser);

                //trackinguser = new Trackinguser(buyselltransactions.User_id, DateTime.Now, 1, "buyselltransactions", (int)buyselltransactions.Id);
                //trackinguser.Attribute = "thesis";
                //trackinguser.Oldvalue = "";
                //trackinguser.Newvalue = buyselltransactions.Thesis.ToString();
                //_context.Trackingusers.Add(trackinguser);

                //_context.SaveChanges();




            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok(buyselltransactions);
        }

        // DELETE: api/Buyselltransactions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBuyselltransactions([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var buyselltransactions = await _context.Buyselltransactions.FindAsync(id);
            if (buyselltransactions == null)
            {
                return NotFound();
            }

            _context.Buyselltransactions.Remove(buyselltransactions);
            await _context.SaveChangesAsync();

            return Ok(buyselltransactions);
        }

        private bool BuyselltransactionsExists(int id)
        {
            return _context.Buyselltransactions.Any(e => e.Id == id);
        }
    }

    public class SymbolInAccount
    {
        public int Account_id { get; set; }
        public int Symbol_id { get; set; }
        public int Volume { get; set; }
    }
}