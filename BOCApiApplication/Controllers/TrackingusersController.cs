﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrackingusersController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public TrackingusersController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Trackingusers
        [HttpGet]
        public IEnumerable<Trackinguser> GetTrackingusers()
        {
            return _context.Trackingusers;
        }

        // GET: api/Trackingusers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrackinguser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var trackinguser = await _context.Trackingusers.FindAsync(id);

            if (trackinguser == null)
            {
                return NotFound();
            }

            return Ok(trackinguser);
        }

        // PUT: api/Trackingusers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrackinguser([FromRoute] int id, [FromBody] Trackinguser trackinguser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trackinguser.Id)
            {
                return BadRequest();
            }

            _context.Entry(trackinguser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrackinguserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Trackingusers
        [HttpPost]
        public async Task<IActionResult> PostTrackinguser([FromBody] Trackinguser trackinguser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Trackingusers.Add(trackinguser);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTrackinguser", new { id = trackinguser.Id }, trackinguser);
        }

        // DELETE: api/Trackingusers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrackinguser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var trackinguser = await _context.Trackingusers.FindAsync(id);
            if (trackinguser == null)
            {
                return NotFound();
            }

            _context.Trackingusers.Remove(trackinguser);
            await _context.SaveChangesAsync();

            return Ok(trackinguser);
        }

        private bool TrackinguserExists(int id)
        {
            return _context.Trackingusers.Any(e => e.Id == id);
        }
    }
}