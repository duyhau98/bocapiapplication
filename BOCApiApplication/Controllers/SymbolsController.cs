﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;
using BOCApiApplication.Helpers;
using Microsoft.Extensions.Caching.Memory;
using BOCApiApplication.Controllers.Admin;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SymbolsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public SymbolsController(DatabaseContext context)
        {
            _context = context;
        }


        // GET: api/Symbols
        [HttpGet]
        public IEnumerable<Symbol> GetSymbols()
        {
            return (IEnumerable<Symbol>)MemoryCaching.getSymbols();
        }

        [HttpGet("/api/list-symbols-favisualization")]
        public IEnumerable<ListSymbolPreWL_WL> Get()
        {
            List<ListSymbolPreWL_WL> listSymbolPreWL_WLs = new List<ListSymbolPreWL_WL>();
            foreach (var item in _context.Symbols)
            {
                ListSymbolPreWL_WL listSymbolPreWL_WL = new ListSymbolPreWL_WL();
                if (_context.BindSymbolWatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Watchlist_id == 1).Any() && _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Active == true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).FirstOrDefault().Id;
                }
                else if (_context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Active == true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).FirstOrDefault().Id;
                }
                else if (_context.BindSymbolWatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Watchlist_id == 1).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                }
                else
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                }
                listSymbolPreWL_WLs.Add(listSymbolPreWL_WL);
            }

            return listSymbolPreWL_WLs;
        }

        // GET: api/Symbols/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSymbol([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var symbol = await _context.Symbols.FindAsync(id);

            if (symbol == null)
            {
                return NotFound();
            }

            return Ok(symbol);
        }

        // PUT: api/Symbols/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSymbol([FromRoute] int id, [FromBody] Symbol symbol)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != symbol.Id)
            {
                return BadRequest();
            }

            _context.Entry(symbol).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SymbolExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Symbols
        [HttpPost]
        public async Task<IActionResult> PostSymbol([FromBody] Symbol symbol)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Symbols.Add(symbol);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSymbol", new { id = symbol.Id }, symbol);
        }

        // DELETE: api/Symbols/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSymbol([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var symbol = await _context.Symbols.FindAsync(id);
            if (symbol == null)
            {
                return NotFound();
            }

            _context.Symbols.Remove(symbol);
            await _context.SaveChangesAsync();

            return Ok(symbol);
        }

        private bool SymbolExists(int id)
        {
            return _context.Symbols.Any(e => e.Id == id);
        }
    }
}