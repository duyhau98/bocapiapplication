﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOCApiApplication.Common;
using BOCApiApplication.Models;
using BOCApiApplication.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly DatabaseContext _context;
        private IUserService _userService;
        public const string SessionKeyName = "_Name";
        public LoginController(DatabaseContext context,IUserService userService)
        {
            _userService = userService;
            _context = context;
        }


        // POST: api/Login
        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        public  IActionResult Post()
        {

                string token = string.Empty;
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    token = reader.ReadToEnd();
                }
                User user =  JsonConvert.DeserializeObject<User>(token);
                string hashPassword = SHA.GenerateSHA512String(user.Password);

                var userLogin = _userService.Authenticate(user.Username, hashPassword, _context.Users.ToList());
                if(userLogin==null)
                {
                    return BadRequest(new { message = "Username or password is incorrect" });
                }



            return Ok(userLogin);
        }

        // PUT: api/Login/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
