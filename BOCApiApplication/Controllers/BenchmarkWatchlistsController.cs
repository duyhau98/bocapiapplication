﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BenchmarkWatchlistsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public BenchmarkWatchlistsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/BenchmarkWatchlists
        [HttpGet]
        public IEnumerable<BenchmarkWatchlist> GetBenchmarkWatchlists()
        {
            return _context.BenchmarkWatchlists;
        }

        // GET: api/BenchmarkWatchlists/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBenchmarkWatchlist([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var benchmarkWatchlist = await _context.BenchmarkWatchlists.FindAsync(id);

            if (benchmarkWatchlist == null)
            {
                return NotFound();
            }

            return Ok(benchmarkWatchlist);
        }

        // PUT: api/BenchmarkWatchlists/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBenchmarkWatchlist([FromRoute] int id, [FromBody] BenchmarkWatchlist benchmarkWatchlist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != benchmarkWatchlist.Id)
            {
                return BadRequest();
            }

            _context.Entry(benchmarkWatchlist).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BenchmarkWatchlistExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BenchmarkWatchlists
        [HttpPost]
        public async Task<IActionResult> PostBenchmarkWatchlist([FromBody] BenchmarkWatchlist benchmarkWatchlist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.BenchmarkWatchlists.Add(benchmarkWatchlist);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBenchmarkWatchlist", new { id = benchmarkWatchlist.Id }, benchmarkWatchlist);
        }

        // DELETE: api/BenchmarkWatchlists/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBenchmarkWatchlist([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var benchmarkWatchlist = await _context.BenchmarkWatchlists.FindAsync(id);
            if (benchmarkWatchlist == null)
            {
                return NotFound();
            }

            _context.BenchmarkWatchlists.Remove(benchmarkWatchlist);
            await _context.SaveChangesAsync();

            return Ok(benchmarkWatchlist);
        }

        private bool BenchmarkWatchlistExists(int id)
        {
            return _context.BenchmarkWatchlists.Any(e => e.Id == id);
        }
    }
}