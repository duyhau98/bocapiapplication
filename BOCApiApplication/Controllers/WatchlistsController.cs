﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WatchlistsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public WatchlistsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Watchlists
        [HttpGet]
        public IEnumerable<Watchlist> GetWatchlists()
        {
            return _context.Watchlists;
        }

        // GET: api/Watchlists/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetWatchlist([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var watchlist = await _context.Watchlists.FindAsync(id);

            if (watchlist == null)
            {
                return NotFound();
            }

            return Ok(watchlist);
        }

        // PUT: api/Watchlists/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWatchlist([FromRoute] int id, [FromBody] Watchlist watchlist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != watchlist.Id)
            {
                return BadRequest();
            }

            _context.Entry(watchlist).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WatchlistExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Watchlists
        [HttpPost]
        public Watchlist PostWatchlist()
        {
            string jsonModel = string.Empty;
            
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                jsonModel = reader.ReadToEnd();
            }
            Watchlist watchlist = null ;
            if (!string.IsNullOrEmpty(jsonModel))
            {
                    watchlist = JsonConvert.DeserializeObject<Watchlist>(jsonModel);
                    _context.Watchlists.Add(watchlist);
                    _context.SaveChanges();

            }
            return watchlist;
        }

        // DELETE: api/Watchlists/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWatchlist([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var watchlist = await _context.Watchlists.FindAsync(id);
            if (watchlist == null)
            {
                return null;
            }

            _context.Watchlists.Remove(watchlist);
            await _context.SaveChangesAsync();

            return Ok(watchlist);
        }

        private bool WatchlistExists(int id)
        {
            return _context.Watchlists.Any(e => e.Id == id);
        }
    }
}