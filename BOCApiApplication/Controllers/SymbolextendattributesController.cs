﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SymbolextendattributesController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public SymbolextendattributesController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Symbolextendattributes
        [HttpGet]
        public IEnumerable<Symbolextendattributes> GetSymbolextendattributes()
        {
            return _context.Symbolextendattributes;
        }

        // GET: api/Symbolextendattributes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSymbolextendattributes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var symbolextendattributes = await _context.Symbolextendattributes.FindAsync(id);

            if (symbolextendattributes == null)
            {
                return NotFound();
            }

            return Ok(symbolextendattributes);
        }

        // PUT: api/Symbolextendattributes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSymbolextendattributes([FromRoute] int id, [FromBody] Symbolextendattributes symbolextendattributes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != symbolextendattributes.Id)
            {
                return BadRequest();
            }

            _context.Entry(symbolextendattributes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SymbolextendattributesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Symbolextendattributes
        [HttpPost]
        public async Task<IActionResult> PostSymbolextendattributes([FromBody] Symbolextendattributes symbolextendattributes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Symbolextendattributes.Add(symbolextendattributes);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSymbolextendattributes", new { id = symbolextendattributes.Id }, symbolextendattributes);
        }

        // DELETE: api/Symbolextendattributes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSymbolextendattributes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var symbolextendattributes = await _context.Symbolextendattributes.FindAsync(id);
            if (symbolextendattributes == null)
            {
                return NotFound();
            }

            _context.Symbolextendattributes.Remove(symbolextendattributes);
            await _context.SaveChangesAsync();

            return Ok(symbolextendattributes);
        }

        private bool SymbolextendattributesExists(int id)
        {
            return _context.Symbolextendattributes.Any(e => e.Id == id);
        }
    }
}