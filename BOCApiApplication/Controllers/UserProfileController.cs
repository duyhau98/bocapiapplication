﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOCApiApplication.Common;
using BOCApiApplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public UserProfileController(DatabaseContext context)
        {
            _context = context;
        }



        // POST: api/UserProfile
        [HttpPost]
        public User Post()
        {
            string token = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                token = reader.ReadToEnd();
            }
            TokenJson tokenJson = JsonConvert.DeserializeObject<TokenJson>(token);
            if(tokenJson.Token!=null)
            {
                var key = Encoding.ASCII.GetBytes(Authentication.Secrets);
                var handler = new JwtSecurityTokenHandler();
                var validations = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
                var claims = handler.ValidateToken(tokenJson.Token, validations, out var tokenSecure);

                var user = _context.Users.Find(int.Parse(claims.Identity.Name));

                return user;
            } else
            {
                return null;
            }
        }

        // PUT: api/UserProfile/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
    public class TokenJson
    {
        public string Token { get; set; }
    }
}
