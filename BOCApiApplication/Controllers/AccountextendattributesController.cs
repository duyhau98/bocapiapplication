﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountextendattributesController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public AccountextendattributesController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Accountextendattributes
        [HttpGet]
        public IEnumerable<Accountextendattributes> GetAccountextendattributes()
        {
            return _context.Accountextendattributes;
        }

        // GET: api/Accountextendattributes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAccountextendattributes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var accountextendattributes = await _context.Accountextendattributes.FindAsync(id);

            if (accountextendattributes == null)
            {
                return NotFound();
            }

            return Ok(accountextendattributes);
        }

        // PUT: api/Accountextendattributes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccountextendattributes([FromRoute] int id, [FromBody] Accountextendattributes accountextendattributes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != accountextendattributes.Id)
            {
                return BadRequest();
            }

            _context.Entry(accountextendattributes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountextendattributesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Accountextendattributes
        [HttpPost]
        public async Task<IActionResult> PostAccountextendattributes([FromBody] Accountextendattributes accountextendattributes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Accountextendattributes.Add(accountextendattributes);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAccountextendattributes", new { id = accountextendattributes.Id }, accountextendattributes);
        }

        // DELETE: api/Accountextendattributes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccountextendattributes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var accountextendattributes = await _context.Accountextendattributes.FindAsync(id);
            if (accountextendattributes == null)
            {
                return NotFound();
            }

            _context.Accountextendattributes.Remove(accountextendattributes);
            await _context.SaveChangesAsync();

            return Ok(accountextendattributes);
        }

        private bool AccountextendattributesExists(int id)
        {
            return _context.Accountextendattributes.Any(e => e.Id == id);
        }
    }
}