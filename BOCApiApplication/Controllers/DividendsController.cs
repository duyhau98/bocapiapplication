﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DividendsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public DividendsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Dividends
        [HttpGet]
        public IEnumerable<Dividend> GetDividends()
        {
            return _context.Dividends;
        }

        // GET: api/Dividends/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDividend([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dividend = await _context.Dividends.FindAsync(id);

            if (dividend == null)
            {
                return NotFound();
            }

            return Ok(dividend);
        }

        // PUT: api/Dividends/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDividend([FromRoute] int id, [FromBody] Dividend dividend)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dividend.Id)
            {
                return BadRequest();
            }

            _context.Entry(dividend).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DividendExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Dividends
        [HttpPost]
        public async Task<IActionResult> PostDividend([FromBody] Dividend dividend)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Dividends.Add(dividend);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDividend", new { id = dividend.Id }, dividend);
        }

        // DELETE: api/Dividends/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDividend([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dividend = await _context.Dividends.FindAsync(id);
            if (dividend == null)
            {
                return NotFound();
            }

            _context.Dividends.Remove(dividend);
            await _context.SaveChangesAsync();

            return Ok(dividend);
        }

        private bool DividendExists(int id)
        {
            return _context.Dividends.Any(e => e.Id == id);
        }
    }
}