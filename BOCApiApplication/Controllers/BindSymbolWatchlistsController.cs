﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BindSymbolWatchlistsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public BindSymbolWatchlistsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/BindSymbolWatchlists
        [HttpGet]
        public IEnumerable<BindSymbolWatchlist> GetBindSymbolWatchlists()
        {
            return _context.BindSymbolWatchlists;
        }

        // GET: api/BindSymbolWatchlists/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBindSymbolWatchlist([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bindSymbolWatchlist = await _context.BindSymbolWatchlists.FindAsync(id);

            if (bindSymbolWatchlist == null)
            {
                return NotFound();
            }

            return Ok(bindSymbolWatchlist);
        }

        // PUT: api/BindSymbolWatchlists/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBindSymbolWatchlist([FromRoute] int id, [FromBody] BindSymbolWatchlist bindSymbolWatchlist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bindSymbolWatchlist.Id)
            {
                return BadRequest();
            }

            _context.Entry(bindSymbolWatchlist).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BindSymbolWatchlistExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BindSymbolWatchlists
        [HttpPost]
        public async Task<IActionResult> PostBindSymbolWatchlist([FromBody] BindSymbolWatchlist bindSymbolWatchlist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.BindSymbolWatchlists.Add(bindSymbolWatchlist);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBindSymbolWatchlist", new { id = bindSymbolWatchlist.Id }, bindSymbolWatchlist);
        }

        // DELETE: api/BindSymbolWatchlists/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBindSymbolWatchlist([FromRoute] int id)
        {
            if(id!=0)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var bindSymbolWatchlist = await _context.BindSymbolWatchlists.FindAsync(id);
                if (bindSymbolWatchlist == null)
                {
                    return NotFound();
                }

                _context.BindSymbolWatchlists.Remove(bindSymbolWatchlist);
                await _context.SaveChangesAsync();

                return Ok(bindSymbolWatchlist);
            } else
            {
                string jsonModel = string.Empty;
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    jsonModel = reader.ReadToEnd();
                }
                BindSymbolWatchlist itemBindSymbolWL = JsonConvert.DeserializeObject<BindSymbolWatchlist>(jsonModel);
                var bindSymbolWatchlist =  _context.BindSymbolWatchlists.ToList().Where(x => x.Watchlist_id == itemBindSymbolWL.Watchlist_id && x.User_id == itemBindSymbolWL.User_id && x.Symbol_id == itemBindSymbolWL.Symbol_id).FirstOrDefault();
                itemBindSymbolWL.Id = bindSymbolWatchlist.Id;
                _context.BindSymbolWatchlists.Remove(bindSymbolWatchlist);
                _context.SaveChanges();
                return Ok(itemBindSymbolWL);
            }

        }

        private bool BindSymbolWatchlistExists(int id)
        {
            return _context.BindSymbolWatchlists.Any(e => e.Id == id);
        }
    }

}