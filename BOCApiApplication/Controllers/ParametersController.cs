﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParametersController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public ParametersController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Parameters
        [HttpGet]
        public IEnumerable<Parameters> GetParameters()
        {
            return _context.Parameters;
        }

        // GET: api/Parameters/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetParameters([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parameters = await _context.Parameters.FindAsync(id);

            if (parameters == null)
            {
                return NotFound();
            }

            return Ok(parameters);
        }

        // PUT: api/Parameters/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParameters([FromRoute] int id, [FromBody] Parameters parameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parameters.Id)
            {
                return BadRequest();
            }

            _context.Entry(parameters).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParametersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Parameters
        [HttpPost]
        public async Task<IActionResult> PostParameters([FromBody] Parameters parameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Parameters.Add(parameters);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetParameters", new { id = parameters.Id }, parameters);
        }

        // DELETE: api/Parameters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteParameters([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parameters = await _context.Parameters.FindAsync(id);
            if (parameters == null)
            {
                return NotFound();
            }

            _context.Parameters.Remove(parameters);
            await _context.SaveChangesAsync();

            return Ok(parameters);
        }

        private bool ParametersExists(int id)
        {
            return _context.Parameters.Any(e => e.Id == id);
        }
    }
}