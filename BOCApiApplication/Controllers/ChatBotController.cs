﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BOCApiApplication.Helpers;
using BOCApiApplication.Interfaces;
using BOCApiApplication.Models;
using Google.Cloud.Dialogflow.V2;
using Google.Protobuf;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Args;


namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChatBotController : ControllerBase
    {
        private readonly DatabaseContext _context;
        public static IMemoryCache _cache;
        public ISystemService _systemService;

        private static readonly JsonParser jsonParser =new JsonParser(JsonParser.Settings.Default.WithIgnoreUnknownFields(true));

        [HttpPost("/api/chat-bot")]
        public IActionResult DialogAction()
        {
            WebhookRequest request;
            using (var reader = new StreamReader(Request.Body))
            {
                request = jsonParser.Parse<WebhookRequest>(reader);
            }

            var pas = request.QueryResult.Parameters;
            var question = request.QueryResult.QueryText;

            var askingName = pas.Fields.ContainsKey("name") && pas.Fields["name"].ToString().Replace('\"', ' ').Trim().Length > 0;
            var askingInformation = pas.Fields.ContainsKey("information") && pas.Fields["information"].ToString().Replace('\"', ' ').Trim().Length > 0;

            var response = new WebhookResponse();

            StringBuilder sb = new StringBuilder();

            if (question.ToLower().Contains("information"))
            {
                foreach (var item in _context.Symbols)
                {
                    if(question.ToLower().Contains(" "+ item.Symbolname.ToLower()))
                    {
                        sb.Append("The name of symbol is: " + item.Symbolname + "; ");
                        sb.Append("The fscore of symbol is: " + item.Fscore + "; ");
                        sb.Append("The mscore of symbol is: " + item.Mscore + "; ");
                    }
                }

                response.FulfillmentText = sb.ToString();
                if (string.IsNullOrEmpty(sb.ToString().Trim()))
                {
                    response.FulfillmentText = "Could not find it!!";
                }
            }

            return Ok(response);
        }
        public ChatBotController(DatabaseContext context, IMemoryCache cache, ISystemService systemService)
        {
            _systemService = systemService;
            _context = context;
            _cache = cache;

            MemoryCaching._cache = cache;
            MemoryCaching._context = context;
        }

        //Bot Telegrams

        private static readonly TelegramBotClient Bot = new TelegramBotClient("1036513627:AAFC3H_60-vhU4TXRGXKNwbLKIQo1B8aBDs");
        // GET api/chatbot
        [HttpGet]
        public string Get()
        {
            // Cache các bảng đầu chương trình.
            MemoryCaching.CacheSymbols();
            //string kl = _systemService.TestService();
            //// Mẫu code lấy cache
            //var b = MemoryCaching._cache.Get("Symbols");

            Bot.OnMessage += Bot_OnMesasg;
            Bot.StartReceiving();
            Console.ReadLine();

            Bot.StopReceiving();
            return "Hi CHat Bot";
        }

        private  void Bot_OnMesasg(object sender, MessageEventArgs e)
        {
            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {

                StringBuilder sb = new StringBuilder();
                bool isHave = false;
                foreach (var item in _context.Symbols)
                {
                    if (e.Message.Text.ToLower().Contains(item.Symbolname.ToLower()))
                    {
                        isHave = true;
                        sb.Append(item.Symbolname +  " tên công ty: " + item.Companyshortname + "; ");
                        sb.Append("F-score : " + item.Fscore + "; ");
                        sb.Append("M-score : " + item.Mscore + "; ");
                        Bot.SendTextMessageAsync(e.Message.Chat.Id, sb.ToString());
                    }
                }
                if(isHave==false)
                {
                    sb.Append("Can not find symbol!!!");
                    Bot.SendTextMessageAsync(e.Message.Chat.Id, sb.ToString());
                }
            }
        }

        [HttpGet("/api/chatbot/batch-job-symbol-{symbolName}")]
        public string BatchJotChatBot(string symbolName)
        {
            // id anh Hien 637636302
            // id Hau  730851052
            string userApiTelegramBot = getJsonFromURL("https://api.telegram.org/bot1036513627:AAFC3H_60-vhU4TXRGXKNwbLKIQo1B8aBDs/getUpdates");
            StringBuilder sb = new StringBuilder();
            var symbol = _context.Symbols.Where(x => x.Symbolname == symbolName).SingleOrDefault();
            if (symbol!=null)
            {
                sb.Append(symbol.Symbolname + " tên công ty: " + symbol.Companyshortname + "; ");
                sb.Append("F-score : " + symbol.Fscore + "; ");
                sb.Append("M-score : " + symbol.Mscore + "; ");
            } else
            {
                sb.Append("Cannot find symbol!!");
            }
            Bot.SendTextMessageAsync(730851052, sb.ToString());
            Bot.SendTextMessageAsync(637636302, sb.ToString());

            Bot.StartReceiving();

            Bot.StopReceiving();

            return "Hi";
        }

        // Returns JSON string
        string getJsonFromURL(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
