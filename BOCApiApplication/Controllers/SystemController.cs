﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BOCApiApplication.Interfaces;
using BOCApiApplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemController : ControllerBase
    {
        private readonly DatabaseContext _context;
        private readonly ISystemService _systemService; // Khai báo systemService đẻ gọi hàm từ SystemService
        public SystemController(DatabaseContext context, ISystemService systemService)
        {
            _systemService = systemService;
            _context = context;
        }
        // GET: api/System
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("/api/batch-job-{typeBatchJob}")]
        public string Get(int? typeBatchJob)
        {
            switch(typeBatchJob)
            {
                case 1:
                    //do batch job 01
                case 2:
                    //do batch job 02
                default:
                    return null;
            }
        }

        // GET: api/System/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/System
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/System/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
