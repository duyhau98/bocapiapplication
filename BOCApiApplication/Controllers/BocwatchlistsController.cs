﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BocwatchlistsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public BocwatchlistsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Bocwatchlists
        [HttpGet]
        public IEnumerable<Bocwatchlist> GetBocwatchlists()
        {
            return _context.Bocwatchlists;
        }

        // GET: api/Bocwatchlists/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBocwatchlist([FromRoute] int? id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bocwatchlist = await _context.Bocwatchlists.FindAsync(id);

            if (bocwatchlist == null)
            {
                return NotFound();
            }

            return Ok(bocwatchlist);
        }

        // PUT: api/Bocwatchlists/5
        [HttpPut("{id}-{userId}")]
        public async Task<IActionResult> PutBocwatchlist([FromRoute] int? id, [FromRoute] int? userId, [FromBody] Bocwatchlist bocwatchlist)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bocwatchlist.Id)
            {
                return BadRequest();
            }
            var bocWatchlistDB = _context.Bocwatchlists.Find(id);

            // Cần user_id ???
            int user_id = (int)userId;
            Trackinguser trackinguser = new Trackinguser(user_id, DateTime.Now, 3, "bocwatchlist", (int)id);
            if (bocWatchlistDB.Fafuture != bocwatchlist.Fafuture)
            {
                trackinguser.Attribute = "fafuture";
                trackinguser.Oldvalue = bocWatchlistDB.Fafuture.ToString();
                trackinguser.Newvalue = bocwatchlist.Fafuture.ToString();
                _context.Trackingusers.Add(trackinguser);
                _context.SaveChanges();
            }

            trackinguser = new Trackinguser(user_id, DateTime.Now, 3, "bocwatchlist", (int)id);
            if (bocWatchlistDB.Fapast != bocwatchlist.Fapast)
            {


                trackinguser.Attribute = "fapast";
                trackinguser.Oldvalue = bocWatchlistDB.Fapast.ToString();
                trackinguser.Newvalue = bocwatchlist.Fapast.ToString();
                _context.Trackingusers.Add(trackinguser);
                _context.SaveChanges();
            }

            trackinguser = new Trackinguser(user_id, DateTime.Now, 3, "bocwatchlist", (int)id);
            if (bocWatchlistDB.Farisk != bocwatchlist.Farisk)
            {
                trackinguser.Attribute = "farisk";
                trackinguser.Oldvalue = bocWatchlistDB.Farisk.ToString();
                trackinguser.Newvalue = bocwatchlist.Farisk.ToString();
                _context.Trackingusers.Add(trackinguser);
                _context.SaveChanges();

            }

            trackinguser = new Trackinguser(user_id, DateTime.Now, 3, "bocwatchlist", (int)id);
            if (bocWatchlistDB.Fatargetprice != bocwatchlist.Fatargetprice)
            {
                trackinguser.Attribute = "fatargetprice";
                trackinguser.Oldvalue = bocWatchlistDB.Fatargetprice.ToString();
                trackinguser.Newvalue = bocwatchlist.Fatargetprice.ToString();
                _context.Trackingusers.Add(trackinguser);
                _context.SaveChanges();
                bocWatchlistDB.Fatargetpricedate = bocwatchlist.Fatargetpricedate;
            }

            trackinguser = new Trackinguser(user_id, DateTime.Now, 3, "bocwatchlist", (int)id);
            if (bocWatchlistDB.Taneelyelliott != bocwatchlist.Taneelyelliott)
            {
                trackinguser.Attribute = "taneelyelliott";
                trackinguser.Oldvalue = bocWatchlistDB.Taneelyelliott.ToString();
                trackinguser.Newvalue = bocwatchlist.Taneelyelliott.ToString();
                _context.Trackingusers.Add(trackinguser);
                _context.SaveChanges();

            }

            trackinguser = new Trackinguser(user_id, DateTime.Now, 3, "bocwatchlist", (int)id);
            if (bocWatchlistDB.Tapatern != bocwatchlist.Tapatern)
            {
                trackinguser.Attribute = "tapatern";
                trackinguser.Oldvalue = bocWatchlistDB.Tapatern.ToString();
                trackinguser.Newvalue = bocwatchlist.Tapatern.ToString();
                _context.Trackingusers.Add(trackinguser);
                _context.SaveChanges();
            }

            trackinguser = new Trackinguser(user_id, DateTime.Now, 3, "bocwatchlist", (int)id);
            if (bocWatchlistDB.Fascoring != bocwatchlist.Fascoring)
            {
                trackinguser.Attribute = "fascoring";
                trackinguser.Oldvalue = bocWatchlistDB.Fascoring.ToString();
                trackinguser.Newvalue = bocwatchlist.Fascoring.ToString();
                _context.Trackingusers.Add(trackinguser);
                _context.SaveChanges();
            }

            trackinguser = new Trackinguser(user_id, DateTime.Now, 3, "bocwatchlist", (int)id);
            if (bocWatchlistDB.Tascoring != bocwatchlist.Tascoring)
            {
                trackinguser.Attribute = "tascoring";
                trackinguser.Oldvalue = bocWatchlistDB.Tascoring.ToString();
                trackinguser.Newvalue = bocwatchlist.Tascoring.ToString();
                _context.Trackingusers.Add(trackinguser);
                _context.SaveChanges();
            }


                trackinguser = new Trackinguser(user_id, DateTime.Now, 3, "bocwatchlist", (int)id);
                if (bocWatchlistDB.Usernote != bocwatchlist.Usernote)
                {
                    trackinguser.Attribute = "usernote";
                    trackinguser.Oldvalue = bocWatchlistDB.Usernote != null ? bocWatchlistDB.Usernote.ToString() : "";
                    trackinguser.Newvalue = bocwatchlist.Usernote !=null? bocwatchlist.Usernote.ToString(): "";
                    _context.Trackingusers.Add(trackinguser);
                    _context.SaveChanges();
                }




            bocWatchlistDB.Fafuture = bocwatchlist.Fafuture;
            bocWatchlistDB.Fapast = bocwatchlist.Fapast;
            bocWatchlistDB.Farisk = bocwatchlist.Farisk;
            bocWatchlistDB.Fatargetprice = bocwatchlist.Fatargetprice;
            bocWatchlistDB.Taneelyelliott = bocwatchlist.Taneelyelliott;
            bocWatchlistDB.Tapatern = bocwatchlist.Tapatern;
            bocWatchlistDB.Fascoring = bocwatchlist.Fascoring;
            bocWatchlistDB.Tascoring = bocwatchlist.Tascoring;
            bocWatchlistDB.Usernote = bocwatchlist.Usernote;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BocwatchlistExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        // POST: api/Bocwatchlists
        [HttpPost("/api/-add-buy-sell-{userId}")]
        public IActionResult PostBuySellBOC(int userId=0) 
        {
            try
            {
                string jsonModel = string.Empty;

                BocBuySell bocBuySell = null;
                BOCUser bocUser = null;
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    jsonModel = reader.ReadToEnd();
                }
                if (!string.IsNullOrEmpty(jsonModel))
                {
                    bocBuySell = JsonConvert.DeserializeObject<BocBuySell>(jsonModel);
                    float? buyOldValue;
                    DateTime? oldExpireddate;
                    float? oldRatio;
                    Bocwatchlist bocwatchlist = _context.Bocwatchlists.Find(bocBuySell.Id);
                    switch (bocBuySell.Buyname)
                    {
                        case "b0":
                            buyOldValue = bocwatchlist.B0;
                            oldExpireddate = bocwatchlist.B0expireddate;
                            oldRatio = bocwatchlist.B0ratio;
                            bocwatchlist.B0expireddate = bocBuySell.Expireddate;
                            bocwatchlist.B0type = bocBuySell.Buytype;
                            bocwatchlist.B0 = bocBuySell.Price;
                            bocwatchlist.B0ratio = bocBuySell.Ratio;

                            _context.SaveChanges();

                            Trackinguser trackinguser = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser.Attribute = bocBuySell.Buyname;
                            trackinguser.Oldvalue = buyOldValue.ToString();
                            trackinguser.Newvalue = bocBuySell.Price.ToString();
                            _context.Trackingusers.Add(trackinguser);

                            trackinguser = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser.Attribute = "b0expireddate";
                            trackinguser.Oldvalue = oldExpireddate.ToString();
                            trackinguser.Newvalue = bocBuySell.Expireddate.ToString();
                            _context.Trackingusers.Add(trackinguser);

                            trackinguser = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser.Attribute = "b0ratio";
                            trackinguser.Oldvalue = oldRatio.ToString();
                            trackinguser.Newvalue = bocBuySell.Ratio.ToString();
                            _context.Trackingusers.Add(trackinguser);

                            _context.SaveChanges();
                            break;
                        case "b1":
                            buyOldValue = bocwatchlist.B1;
                            oldExpireddate = bocwatchlist.B1expireddate;
                            oldRatio = bocwatchlist.B1ratio;
                            bocwatchlist.B1expireddate = bocBuySell.Expireddate;
                            bocwatchlist.B1type = bocBuySell.Buytype;
                            bocwatchlist.B1 = bocBuySell.Price;
                            bocwatchlist.B1ratio = bocBuySell.Ratio;

                            Trackinguser trackinguser1 = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser1.Attribute = bocBuySell.Buyname;
                            trackinguser1.Oldvalue = buyOldValue.ToString();
                            trackinguser1.Newvalue = bocBuySell.Price.ToString();
                            _context.Trackingusers.Add(trackinguser1);

                            trackinguser1 = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser1.Attribute = "b0expireddate";
                            trackinguser1.Oldvalue = oldExpireddate.ToString();
                            trackinguser1.Newvalue = bocBuySell.Expireddate.ToString();
                            _context.Trackingusers.Add(trackinguser1);

                            trackinguser1 = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser1.Attribute = "b0ratio";
                            trackinguser1.Oldvalue = oldRatio.ToString();
                            trackinguser1.Newvalue = bocBuySell.Ratio.ToString();
                            _context.Trackingusers.Add(trackinguser1);

                            _context.SaveChanges();
                            break;
                        case "b2":
                            buyOldValue = bocwatchlist.B2;
                            oldExpireddate = bocwatchlist.B2expireddate;
                            oldRatio = bocwatchlist.B2ratio;
                            bocwatchlist.B2expireddate = bocBuySell.Expireddate;
                            bocwatchlist.B2type = bocBuySell.Buytype;
                            bocwatchlist.B2 = bocBuySell.Price;
                            bocwatchlist.B2ratio = bocBuySell.Ratio;

                            Trackinguser trackinguser2 = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser2.Attribute = bocBuySell.Buyname;
                            trackinguser2.Oldvalue = buyOldValue.ToString();
                            trackinguser2.Newvalue = bocBuySell.Price.ToString();
                            _context.Trackingusers.Add(trackinguser2);

                            trackinguser2 = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser2.Attribute = "b0expireddate";
                            trackinguser2.Oldvalue = oldExpireddate.ToString();
                            trackinguser2.Newvalue = bocBuySell.Expireddate.ToString();
                            _context.Trackingusers.Add(trackinguser2);

                            trackinguser2 = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser2.Attribute = "b0ratio";
                            trackinguser2.Oldvalue = oldRatio.ToString();
                            trackinguser2.Newvalue = bocBuySell.Ratio.ToString();
                            _context.Trackingusers.Add(trackinguser2);

                            _context.SaveChanges();
                            break;
                        case "b3":
                            buyOldValue = bocwatchlist.B3;
                            oldExpireddate = bocwatchlist.B3expireddate;
                            oldRatio = bocwatchlist.B3ratio;
                            bocwatchlist.B3expireddate = bocBuySell.Expireddate;
                            bocwatchlist.B3type = bocBuySell.Buytype;
                            bocwatchlist.B3 = bocBuySell.Price;
                            bocwatchlist.B3ratio = bocBuySell.Ratio;

                            Trackinguser trackinguser3 = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser3.Attribute = bocBuySell.Buyname;
                            trackinguser3.Oldvalue = buyOldValue.ToString();
                            trackinguser3.Newvalue = bocBuySell.Price.ToString();
                            _context.Trackingusers.Add(trackinguser3);

                            trackinguser3 = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser3.Attribute = "b0expireddate";
                            trackinguser3.Oldvalue = oldExpireddate.ToString();
                            trackinguser3.Newvalue = bocBuySell.Expireddate.ToString();
                            _context.Trackingusers.Add(trackinguser3);

                            trackinguser = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguser.Attribute = "b0ratio";
                            trackinguser.Oldvalue = oldRatio.ToString();
                            trackinguser.Newvalue = bocBuySell.Ratio.ToString();
                            _context.Trackingusers.Add(trackinguser);

                            _context.SaveChanges();
                            break;
                        default:
                            buyOldValue = bocwatchlist.B0;
                            oldExpireddate = bocwatchlist.B0expireddate;
                            oldRatio = bocwatchlist.B0ratio;
                            bocwatchlist.B0expireddate = bocBuySell.Expireddate;
                            bocwatchlist.B0type = bocBuySell.Buytype;
                            bocwatchlist.B0 = bocBuySell.Price;
                            bocwatchlist.B0ratio = bocBuySell.Ratio;

                            Trackinguser trackinguserDefault = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguserDefault.Attribute = bocBuySell.Buyname;
                            trackinguserDefault.Oldvalue = buyOldValue.ToString();
                            trackinguserDefault.Newvalue = bocBuySell.Price.ToString();
                            _context.Trackingusers.Add(trackinguserDefault);

                            trackinguserDefault = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguserDefault.Attribute = "b0expireddate";
                            trackinguserDefault.Oldvalue = oldExpireddate.ToString();
                            trackinguserDefault.Newvalue = bocBuySell.Expireddate.ToString();
                            _context.Trackingusers.Add(trackinguserDefault);

                            trackinguserDefault = new Trackinguser(userId, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                            trackinguserDefault.Attribute = "b0ratio";
                            trackinguserDefault.Oldvalue = oldRatio.ToString();
                            trackinguserDefault.Newvalue = bocBuySell.Ratio.ToString();
                            _context.Trackingusers.Add(trackinguserDefault);

                            _context.SaveChanges();
                            break;

                    }
                    return Ok();
                }
            }catch(Exception ex)
            {
                return BadRequest();
            }
            return Ok();

        }

        // POST: api/Bocwatchlists
        [HttpPost]
        public IActionResult PostBocwatchlist()
        {
            string jsonModel = string.Empty;

            Bocwatchlist bocwatchlist = null;
            BOCUser bocUser = null;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                jsonModel = reader.ReadToEnd();
            }
            if (!string.IsNullOrEmpty(jsonModel))
            {
                bocwatchlist = JsonConvert.DeserializeObject<Bocwatchlist>(jsonModel);
                bocUser = JsonConvert.DeserializeObject<BOCUser>(jsonModel);
                if(!_context.Bocwatchlists.Where(x=>x.Symbol_id==bocwatchlist.Symbol_id).Any())
                {
                    _context.Bocwatchlists.Add(bocwatchlist);
                    _context.SaveChanges();
                    Trackinguser trackinguser = new Trackinguser(bocUser.User_id, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlist.Id);
                    trackinguser.Attribute = "active";
                    trackinguser.Oldvalue = "0";
                    trackinguser.Newvalue = "1";
                    _context.Trackingusers.Add(trackinguser);
                } else
                {

                    Bocwatchlist bocwatchlistEdit = _context.Bocwatchlists.Where(x=>x.Symbol_id==bocwatchlist.Symbol_id).SingleOrDefault();
                    Trackinguser trackinguser = new Trackinguser(bocUser.User_id, DateTime.Now, 1, "bocwatchlist", (int)bocwatchlistEdit.Id);
                    trackinguser.Attribute = "active";
                    trackinguser.Oldvalue = "0";
                    trackinguser.Newvalue = "1";
                    _context.Trackingusers.Add(trackinguser);
                    bocwatchlistEdit.Active = true;
                    bocwatchlistEdit.Activedate = DateTime.Now;
                }

                _context.SaveChanges();
            }

            return Ok(bocwatchlist);
        }

        // DELETE: api/Bocwatchlists/5
        [HttpDelete("{id}-{userId}")]
        public async Task<IActionResult> DeleteBocwatchlist([FromRoute] int? id, [FromRoute] int? userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bocwatchlist = await _context.Bocwatchlists.FindAsync(id);
            if (bocwatchlist == null)
            {
                return NotFound();
            }

            //_context.Bocwatchlists.Remove(bocwatchlist);
            bocwatchlist.Active = false;
            bocwatchlist.Taneelyelliott = 0;
            bocwatchlist.Tapatern = 0;
            bocwatchlist.Tascoring = 0;
            bocwatchlist.Tatargetprice = 0;
            bocwatchlist.Fafuture = 0;
            bocwatchlist.Fapast = 0;
            bocwatchlist.Farisk = 0;
            bocwatchlist.Fascoring = 0;
            bocwatchlist.Fatargetprice = 0;
            bocwatchlist.Inactivedate = DateTime.Now;
            
            int user_id = (int) userId; 
            Trackinguser trackinguser = new Trackinguser(user_id,DateTime.Now, 2, "bocwatchlist", (int)id);
            trackinguser.Attribute = "active";
            trackinguser.Oldvalue = "1";
            trackinguser.Newvalue = "0";
            _context.Trackingusers.Add(trackinguser);
            await _context.SaveChangesAsync();

            return Ok(bocwatchlist);
        }

        private bool BocwatchlistExists(int? id)
        {
            return _context.Bocwatchlists.Any(e => e.Id == id);
        }
    }
    public class BOCUser
    {
        public int User_id { get; set; }
    }

    public class BocBuySell
    {
        public Nullable<int> Id { get; set; }
        public string Buyname { get; set; }
        public string Buytype { get; set; }
        public Nullable<float> Price { get; set; }
        public Nullable<float> Ratio { get; set; }
        public Nullable<DateTime> Expireddate { get; set; }

    }
}