﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOCApiApplication.Common;
using BOCApiApplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BOCApiApplication.Controllers.Admin
{
    [Route("api/[controller]")]
    [ApiController]
    public class ListSymbolController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public ListSymbolController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/ListSymbol
        [HttpGet]
        public IEnumerable<ListSymbolPreWL_WL> Get()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("symbolnumber", 10);
            string listSymbolJson = Utilities.ExecuteStoreProcedure("GET_TOP_SYMBOL_BY_FSCORE", parameters);
            List<Symbol> listSymbol = JsonConvert.DeserializeObject<List<Symbol>>(listSymbolJson);
            List<ListSymbolPreWL_WL> listSymbolPreWL_WLs = new List<ListSymbolPreWL_WL>();
            foreach (var item in listSymbol)
            {
                ListSymbolPreWL_WL listSymbolPreWL_WL = new ListSymbolPreWL_WL();
                if (_context.BindSymbolWatchlists.ToList().Where(x=>x.Symbol_id==item.Id&&x.Watchlist_id==1).Any() && _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id&&x.Active==true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).FirstOrDefault().Id;
                } else if (_context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Active == true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).FirstOrDefault().Id;
                } else if(_context.BindSymbolWatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Watchlist_id == 1 ).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                } else
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                }
                listSymbolPreWL_WLs.Add(listSymbolPreWL_WL);
            }
            return listSymbolPreWL_WLs;
        }

        [HttpGet("/api/top-{count}-{type}")]
        public IEnumerable<ListSymbolPreWL_WL> GetTopSymbols(int count = 0, string type = "")
        {
            string listSymbolJson = "";
            if (type=="fscore")
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("symbolnumber", count);
                listSymbolJson = Utilities.ExecuteStoreProcedure("GET_TOP_SYMBOL_BY_FSCORE", parameters);
            }
            if(type=="mscore")
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("symbolnumber", count);
                listSymbolJson = Utilities.ExecuteStoreProcedure("GET_TOP_SYMBOL_BY_MSCORE", parameters);
            }
            List<Symbol> listSymbol = JsonConvert.DeserializeObject<List<Symbol>>(listSymbolJson);
            List<ListSymbolPreWL_WL> listSymbolPreWL_WLs = new List<ListSymbolPreWL_WL>();
            foreach (var item in listSymbol)
            {
                ListSymbolPreWL_WL listSymbolPreWL_WL = new ListSymbolPreWL_WL();
                if (_context.BindSymbolWatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Watchlist_id == 1).Any() && _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Active == true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).FirstOrDefault().Id;
                }
                else if (_context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Active == true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).FirstOrDefault().Id;
                }
                else if (_context.BindSymbolWatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Watchlist_id == 1).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                }
                else
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                }
                listSymbolPreWL_WLs.Add(listSymbolPreWL_WL);
            }
            return listSymbolPreWL_WLs;
        }

        [HttpGet("/api/list-symbol-{mscorenumber}-mscore-from-top-{topfscore}-fscore")]
        public IEnumerable<ListSymbolPreWL_WL> TopMScoreFromFscore(int mscorenumber=0 , int topfscore = 0)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("fscore_symbol_number", topfscore);
            parameters.Add("mscorenumber", mscorenumber);
            string listSymbolJson = Utilities.ExecuteStoreProcedure("GET_TOP_MSCORE_SYMBOL_FROM_FSCORE", parameters);
            List<Symbol> listSymbol = JsonConvert.DeserializeObject<List<Symbol>>(listSymbolJson);
            List<ListSymbolPreWL_WL> listSymbolPreWL_WLs = new List<ListSymbolPreWL_WL>();
            foreach (var item in listSymbol)
            {
                ListSymbolPreWL_WL listSymbolPreWL_WL = new ListSymbolPreWL_WL();
                if (_context.BindSymbolWatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Watchlist_id == 1).Any() && _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Active == true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).SingleOrDefault().Id;
                }
                else if (_context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Active == true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).SingleOrDefault().Id;
                }
                else if (_context.BindSymbolWatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Watchlist_id == 1).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                }
                else
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                }
                listSymbolPreWL_WLs.Add(listSymbolPreWL_WL);
            }
            return listSymbolPreWL_WLs;
        }

        [HttpGet("/api/list-symbol-{fscorenumber}-fscore-from-top-{topmscore}-mscore")]
        public IEnumerable<ListSymbolPreWL_WL> TopFScoreFromMscore(int fscorenumber = 0, int topmscore =0)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("mscore_symbol_number", topmscore);
            parameters.Add("fscorenumber", fscorenumber);
            string listSymbolJson = Utilities.ExecuteStoreProcedure("GET_TOP_FSCORE_SYMBOL_FROM_MSCORE", parameters);
            List<Symbol> listSymbol = JsonConvert.DeserializeObject<List<Symbol>>(listSymbolJson);
            List<ListSymbolPreWL_WL> listSymbolPreWL_WLs = new List<ListSymbolPreWL_WL>();
            foreach (var item in listSymbol)
            {
                ListSymbolPreWL_WL listSymbolPreWL_WL = new ListSymbolPreWL_WL();
                if (_context.BindSymbolWatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Watchlist_id == 1).Any() && _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Active == true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).SingleOrDefault().Id;
                }
                else if (_context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Active == true).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = true;
                    listSymbolPreWL_WL.BocWatchListId = _context.Bocwatchlists.ToList().Where(x => x.Symbol_id == item.Id).SingleOrDefault().Id;
                }
                else if (_context.BindSymbolWatchlists.ToList().Where(x => x.Symbol_id == item.Id && x.Watchlist_id == 1).Any())
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = true;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                }
                else
                {
                    listSymbolPreWL_WL.Symbol = item;
                    listSymbolPreWL_WL.IsContainPreWL = false;
                    listSymbolPreWL_WL.IsContainBOCWL = false;
                }
                listSymbolPreWL_WLs.Add(listSymbolPreWL_WL);
            }
            return listSymbolPreWL_WLs;
        }

        // GET: api/ListSymbol/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ListSymbol
        [HttpPost]
        public BindSymbolWatchlist Post()
        {
            string jsonModel = string.Empty;
            BindSymbolWatchlist bindSymbolWatchlist = null;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                jsonModel = reader.ReadToEnd();
            }
            if(!string.IsNullOrEmpty(jsonModel))
            {
                bindSymbolWatchlist = JsonConvert.DeserializeObject<BindSymbolWatchlist>(jsonModel);
                _context.BindSymbolWatchlists.Add(bindSymbolWatchlist);
                _context.SaveChanges();
            }
            return bindSymbolWatchlist;
            
        }

        // PUT: api/ListSymbol/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete([FromRoute] int id)
        {
            
        }
    }
    public class ListSymbolPreWL_WL
    {
        public Symbol Symbol { get; set; }
        public bool IsContainBOCWL { get; set; }
        public bool IsContainPreWL { get; set; }
        public Nullable<int> BocWatchListId { get; set; }
    }
}
