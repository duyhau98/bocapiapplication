﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;
using Newtonsoft.Json;
using BOCApiApplication.Common;

namespace BOCApiApplication.Controllers.Admin
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardBOCController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public DashboardBOCController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/DashboardBOC
        [HttpGet]
        public IEnumerable<DashboardBOC> GetDashboardBOC()
        {
            try
            {
              
                var dashboardJson = Common.Utilities.GetViewFromDB("viewbocdashboard");
                var listDashboard = JsonConvert.DeserializeObject<IEnumerable<DashboardBOC>>(dashboardJson);
                return listDashboard;
            } catch(Exception ex)
            {
                return null;
            }
        }

        [HttpGet("/api/tracking-user-boc-dashboard-bocwatchlist-{idbocwacthlist}")]
        public IEnumerable<TrackinguserModel> GetTrackingDashBoard(int idbocwacthlist = 0)
        {
            string listTrackingJson = null;
            List<TrackinguserModel> listTracking = new List<TrackinguserModel>();
            var parameters = new Dictionary<string, object>();
            parameters.Add("idbocwatchlist", idbocwacthlist);
            parameters.Add("attributename", "");
            string[] listAttribute = { "fatargetprice", "fapast", "farisk", "fafuture", "taneelyelliott", "taneelyelliott", "tapatern", "usernote", "b0","b1","b2", "b3" };
            foreach(var item in listAttribute)
            {
                parameters["attributename"] = item;
                listTrackingJson = Utilities.ExecuteStoreProcedure("GET_SYMBOL_TRACKING_USER_BOC_DASHBOARD", parameters);
                listTracking.AddRange(JsonConvert.DeserializeObject<List<TrackinguserModel>>(listTrackingJson));
            }
      
        
            return listTracking;
        }



        // GET: api/DashboardBOC/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDashboardBOC([FromRoute] int? id)
        {


            return Ok(null);
        }

        // PUT: api/DashboardBOC/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDashboardBOC([FromRoute] int? id, [FromBody] DashboardBOC dashboardBOC)
        {

            return NoContent();
        }

        // POST: api/DashboardBOC
        [HttpPost]
        public async Task<IActionResult> PostDashboardBOC([FromBody] DashboardBOC dashboardBOC)
        {


            return Ok();
        }

        // DELETE: api/DashboardBOC/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDashboardBOC([FromRoute] int? id)
        {


            return Ok();
        }


    }
    public class TrackinguserModel {
        public int? IdTrackingUser { get; set; }
        public DateTime Date { get; set; }
        public int Action { get; set; }
        public string Table { get; set; }
        public int Idintable { get; set; }
        public string Attribute { get; set; }
        public string Oldvalue { get; set; }
        public string Newvalue { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }
    }
}
