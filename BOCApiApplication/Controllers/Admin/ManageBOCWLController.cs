﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BOCApiApplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BOCApiApplication.Controllers.Admin
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManageBOCWLController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public ManageBOCWLController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/ManageBOCWL
        [HttpGet("/api/manageBOCWL-{idUser}")]
        public ManageBOCWatchlist Get(int idUser=0)
        {
            string query = "SELECT symbol.symbolname from symbol, bocwatchlist WHERE symbol.id=bocwatchlist.symbol_id GROUP BY symbol.symbolname";
            var dashboardJson = Common.Utilities.ExecuteQuery(query);
            var listSymbolName = JsonConvert.DeserializeObject<List<SymbolName>>(dashboardJson);
            var listWL = _context.Bocwatchlists.Where(x=>x.Active==true).ToList();
            ManageBOCWatchlist bocWL = new ManageBOCWatchlist();
            bocWL.NumberOfSymbols = listWL.GroupBy(x => x.Symbol_id).ToList().Count;
            bocWL.ListSymbolname = listSymbolName;
            bocWL.isDelete = false;
            bocWL.User = _context.Users.Where(x => x.Id == idUser).FirstOrDefault();

            return bocWL;
        }

        // POST: api/ManageBOCWL
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/ManageBOCWL/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
    public class ManageBOCWatchlist
    {

        public Bocwatchlist BOCWatchlist { get; set; }
        public User User { get; set; }
        public List<SymbolName> ListSymbolname { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int NumberOfSymbols { get; set; }
        public Nullable<float> Benchmarkpoint { get; set; }
        public bool isDelete { get; set; }
    }
    public class SymbolName
    {
        public string Symbolname { get; set; }
    }
}
