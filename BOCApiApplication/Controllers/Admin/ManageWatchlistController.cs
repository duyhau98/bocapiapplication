﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BOCApiApplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BOCApiApplication.Controllers.Admin
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManageWatchlistController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public ManageWatchlistController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/ManageWatchlist
        [HttpGet]
        public IEnumerable<ManageWatchlist> Get()
        {
            var listWL = _context.Watchlists.ToList();
            List<ManageWatchlist> listManageWL = new List<ManageWatchlist>();
            ManageWatchlist bocWL = new ManageWatchlist();
            
            foreach (var item in listWL)
            {

                string query = "SELECT symbol.symbolname from bindsymbolwatchlist INNER JOIN symbol ON symbol.id = bindsymbolwatchlist.symbol_id INNER JOIN watchlist ON watchlist.id = bindsymbolwatchlist.watchlist_id WHERE watchlist.id="+ item.Id  +" GROUP BY symbol.symbolname";
                var dashboardJson = Common.Utilities.ExecuteQuery(query);
                var listSymbolName = JsonConvert.DeserializeObject<List<SymbolName>>(dashboardJson);

                ManageWatchlist manageWatchlist = new ManageWatchlist();
                manageWatchlist.Watchlist = item;
                manageWatchlist.CreatedDate = item.Datetime;
                manageWatchlist.User = _context.Users.Find(item.User_id);
                manageWatchlist.NumberOfSymbols = _context.BindSymbolWatchlists.Where(x => x.Watchlist_id == item.Id).Count();
                manageWatchlist.ListSymbolname = listSymbolName;
                if(_context.BenchmarkWatchlists.Where(x => x.Watchlist_id == item.Id).Any())
                {
                    manageWatchlist.Benchmarkpoint = _context.BenchmarkWatchlists.Where(x => x.Watchlist_id == item.Id).SingleOrDefault().Benchmarkpoint;
                }
                if(item.Watchlistname == "Pre-WatchList")
                {
                    manageWatchlist.isDelete = false;
                } else
                {
                    manageWatchlist.isDelete = true;
                }
                listManageWL.Add(manageWatchlist);
            }
            if (listManageWL.Count > 0)
            {
                return listManageWL;
            }
            else
            {
                return null;
            }
        }

        // GET: api/ManageWatchlist/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ManageWatchlist
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/ManageWatchlist/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
    public class ManageWatchlist
    {

        public Watchlist Watchlist { get; set; }
        public User User { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int NumberOfSymbols { get; set; }
        public List<SymbolName> ListSymbolname { get; set; }
        public Nullable<float> Benchmarkpoint { get; set; }
        public bool isDelete { get; set; }
    }
}
