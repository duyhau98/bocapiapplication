﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BOCApiApplication.Models;

namespace BOCApiApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SymboleodsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public SymboleodsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Symboleods
        [HttpGet]
        public IEnumerable<Symboleod> GetSymboleods()
        {
            return _context.Symboleods;
        }

        // GET: api/Symboleods/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSymboleod([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var symboleod = await _context.Symboleods.FindAsync(id);

            if (symboleod == null)
            {
                return NotFound();
            }

            return Ok(symboleod);
        }

        [HttpGet("/api/latest-date")]
        public IActionResult GetLatestDate()
        {
            var date = _context.Symboleods.OrderBy(x => x.Date).ToList();
            return Ok(date[_context.Symboleods.OrderBy(x => x.Date).ToList().Count-1].Date);
        }

        [HttpGet("/api/increase-decrease-symbol-{idSymbol}")]
        public IActionResult GetIncreaseDecreaseSymol(int ?idSymbol)
        {
            if (idSymbol == null)
                return BadRequest();
            try
            {
                var bocWL = _context.Bocwatchlists.Where(x => x.Symbol_id == idSymbol).SingleOrDefault();
                var allSymbolEODById = _context.Symboleods.Where(x => x.Symbol_id == idSymbol).ToList();
                var listSymbolEod = _context.Symboleods.Where(x => x.Symbol_id == idSymbol).OrderBy(x => x.Date).ToList();
                float? recentValue = allSymbolEODById[allSymbolEODById.Count - 1].Close;
                var startSymboleod = listSymbolEod.Where(x => x.Date <= ((DateTime)bocWL.Activedate)).OrderBy(x=>x.Date).ToList();
                if (startSymboleod.Count >0 )
                    return BadRequest("Null");
                float? startValue = startSymboleod[startSymboleod.Count-1].Close;
                return Ok(recentValue / startValue);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
        // PUT: api/Symboleods/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSymboleod([FromRoute] int id, [FromBody] Symboleod symboleod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != symboleod.Id)
            {
                return BadRequest();
            }

            _context.Entry(symboleod).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SymboleodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Symboleods
        [HttpPost]
        public async Task<IActionResult> PostSymboleod([FromBody] Symboleod symboleod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Symboleods.Add(symboleod);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSymboleod", new { id = symboleod.Id }, symboleod);
        }

        // DELETE: api/Symboleods/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSymboleod([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var symboleod = await _context.Symboleods.FindAsync(id);
            if (symboleod == null)
            {
                return NotFound();
            }

            _context.Symboleods.Remove(symboleod);
            await _context.SaveChangesAsync();

            return Ok(symboleod);
        }

        private bool SymboleodExists(int id)
        {
            return _context.Symboleods.Any(e => e.Id == id);
        }
    }
}