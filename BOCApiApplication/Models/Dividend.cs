﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models {
    [Table("dividend")]
    public class Dividend {
        [Key]
        public int Id { get; set; }
        public int Symbol_id { get; set; }
        public DateTime Datetime { get; set; }
        public DateTime Announceddate { get; set; }
        public DateTime Enforcementdate { get; set; }
        public int Type { get; set; }
        public float Ratio { get; set; }
        public float Pricechange { get; set; }
        public float Oldprice { get; set; }
        public float Newprice { get; set; }
    }
}
