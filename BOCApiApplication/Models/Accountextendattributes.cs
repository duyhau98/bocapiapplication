﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models
{
    [Table("accountextendattributes")]
    public class Accountextendattributes
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> Account_id { get; set; }
        public Nullable<DateTime> Datetime { get; set; }
        public string Attributename { get; set; }
        public Nullable<float> Attributenumbet { get; set; }
        public string Attributestring { get; set; }
    }
}
