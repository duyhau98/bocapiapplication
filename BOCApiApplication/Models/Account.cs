﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models
{
    [Table("account")]
    public class Account
    {
        [Key]
        public int Id { get; set; }
        public int User_id { get; set; }
        public Nullable<DateTime> Datetime { get; set; }
        public Nullable<float> Capital { get; set; }
        public Nullable<float> Nav { get; set; }
        public Nullable<float> Cash { get; set; }
        public Nullable<float> Stock { get; set; }
        public Nullable<float> Stockratio { get; set; }
        public Nullable<DateTime> Updateddate { get; set; }
        public string Place { get; set; }
        public string Description { get; set; }
    }
}
