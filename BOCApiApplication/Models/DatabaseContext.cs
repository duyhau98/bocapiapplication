﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BOCApiApplication.Models;

namespace BOCApiApplication.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<BenchmarkWatchlist> BenchmarkWatchlists { get; set; }
        public DbSet<BindSymbolWatchlist> BindSymbolWatchlists { get; set; }
        public DbSet<Dividend> Dividends { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<Parameters> Parameters { get; set; }
        public DbSet<Symbol> Symbols { get; set; }
        public DbSet<Symboleod> Symboleods { get; set; }
        public DbSet<Symbolextendattributes> Symbolextendattributes { get; set; }
        public DbSet<Trackinguser> Trackingusers { get; set; }
        public DbSet<Watchlist> Watchlists { get; set; }
        public DbSet<Bocwatchlist> Bocwatchlists { get; set; }
        public DbSet<Financialstatement> Financialstatements { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Accountextendattributes> Accountextendattributes { get; set; }
        public DbSet<Buyselltransactions> Buyselltransactions { get; set; }
        public DbSet<BOCApiApplication.Models.DashboardBOC> DashboardBOC { get; set; }

    }
}
