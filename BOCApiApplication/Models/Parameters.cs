﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models {
    [Table("parameters")]
    public class Parameters {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Parametersname { get; set; }
        public float Parametersnumber { get; set; }
        public string Parametersstring { get; set; }
    }
}
