﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models
{
    [Table("trackinguser")]
    public class Trackinguser
    {
        [Key]
        public int? Id { get; set; }
        public int User_id { get; set; }
        public DateTime Date { get; set; }
        public int Action { get; set; }
        public string Table { get; set; }
        public int Idintable { get; set; }
        public string Attribute { get; set; }
        public string Oldvalue { get; set; }
        public string Newvalue { get; set; }
        public string Description { get; set; }

        public Trackinguser(int user_id, DateTime dateTime, int action, string table, int idintable)
        {
            User_id = user_id;
            Date = dateTime;
            Action = action;
            Table = table;
            Idintable = idintable;
        }

        public Trackinguser() { }
    }
}
