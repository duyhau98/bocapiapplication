﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models {
    [Table("benchmarkwatchlist")]
    public class BenchmarkWatchlist {
        [Key]
        public int Id { get; set; }
        public int Watchlist_id { get; set; }
        public float Benchmarkpoint { get; set; }
        public DateTime Date { get; set; }

    }
}
