﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models {
    [Table("symbol")]
    public class Symbol {
        [Key]
        public int Id { get; set; }
        public string Symbolname { get; set; }
        public string Companyshortname { get; set; }
        public Nullable<DateTime> Ipodate { get; set; }
        public string Exchange { get; set; }
        public Nullable<int> Field_id { get; set; }
        public Nullable<DateTime> Date { get; set; }
        public string Description { get; set; }
        public int Capitalization { get; set; }
        public Int32 Marketcap { get; set; }
        public Nullable<float> Outstandingshare { get; set; }
        public Nullable<float> Freefloatshare { get; set; }
        public Nullable<float> Freefloat { get; set; }
        public Nullable<float> Peratio { get; set; }
        public Nullable<float> Pedilution { get; set; }
        public Nullable<float> Pbratio { get; set; }
        public Nullable<float> Evebitda { get; set; }
        public Nullable<float> Psratio { get; set; }
        public Nullable<float> Marketprice { get; set; }
        public Nullable<float> Growth { get; set; }
        public Nullable<float> Efficency { get; set; }
        public Nullable<float> Leverage { get; set; }
        public Nullable<float> Margin { get; set; }
        public Nullable<float> Dividend { get; set; }
        public Nullable<float> Value { get; set; }
        public Nullable<float> Fascore { get; set; }
        public Nullable<float> Fscore { get; set; }
        public Nullable<float> Mscore { get; set; }

    }
}
