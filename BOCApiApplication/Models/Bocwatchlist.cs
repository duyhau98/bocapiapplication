﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models
{
    [Table("bocwatchlist")]
    public class Bocwatchlist
    {
        [Key]
        public Nullable<int> Id { get; set; }
        public Nullable<int> Symbol_id { get; set; }
        public Nullable<bool> Active{ get; set; }
        public Nullable<DateTime> Activedate { get; set; }
        public Nullable<DateTime> Inactivedate { get; set; }
        public Nullable<int> Fapast { get; set; }
        public Nullable<int> Farisk { get; set; }
        public Nullable<int> Fafuture { get; set; }
        public Nullable<int> Fascoring { get; set; }
        public Nullable<float> Fatargetprice { get; set; }
        public Nullable<DateTime> Fatargetpricedate { get; set; }
        public Nullable<int> Taneelyelliott { get; set; }
        public Nullable<int> Tapatern { get; set; }
        public Nullable<int> Tascoring { get; set; }
        public Nullable<float> Tatargetprice { get; set; }
        public Nullable<DateTime> Tatargetpricedate { get; set; }
        public Nullable<float> B0 { get; set; }
        public Nullable<DateTime> B0expireddate { get; set; }
        public Nullable<float> B0ratio { get; set; }
        public string B0type { get; set; }
        public Nullable<float> B1 { get; set; }
        public Nullable<DateTime> B1expireddate { get; set; }
        public Nullable<float> B1ratio { get; set; }
        public string B1type { get; set; }
        public Nullable<float> B2 { get; set; }
        public Nullable<DateTime> B2expireddate { get; set; }
        public Nullable<float> B2ratio { get; set; }
        public string B2type { get; set; }
        public Nullable<float> B3 { get; set; }
        public Nullable<DateTime> B3expireddate { get; set; }
        public Nullable<float> B3ratio { get; set; }
        public string B3type { get; set; }
        public Nullable<DateTime> B0date { get; set; }
        public Nullable<DateTime> B1date { get; set; }
        public Nullable<DateTime> B2date { get; set; }
        public Nullable<DateTime> B3date { get; set; }
        public Nullable<float> S1 { get; set; }
        public Nullable<float> S2 { get; set; }
        public Nullable<DateTime> S1date { get; set; }
        public Nullable<DateTime> S2date { get; set; }
        public string Usernote { get; set; }
        public string Description { get; set; }



    }
}
