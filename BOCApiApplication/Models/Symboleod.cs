﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models {
    [Table("symboleod")]
    public class Symboleod {
        [Key]
        public int Id { get; set; }
        public int Symbol_id { get; set; }
        public int Timeframe { get; set; }
        public DateTime Date { get; set; }
        public Nullable<float> Open { get; set; }
        public Nullable<float> Close { get; set; }
        public Nullable<float> High { get; set; }
        public Nullable<float> Low { get; set; }
        public Nullable<Int32> Volume { get; set; }


    }
}
