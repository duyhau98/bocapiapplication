﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models {
    [Table("symbolextendattributes")]
    public class Symbolextendattributes {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Symbol_id { get; set; }
        public int User_id { get; set; }
        public string Attributename { get; set; }
        public float Attributenumber { get; set; }
        public string Attributestring { get; set; }
    }
}
