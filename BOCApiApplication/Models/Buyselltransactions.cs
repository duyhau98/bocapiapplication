﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models
{
    [Table("buyselltransactions")]
    public class Buyselltransactions
    {
        [Key]
        public Nullable<int> Id { get; set; }
        public int Account_id { get; set; }
        public int User_id { get; set; }
        public int Symbol_id { get; set; }
        public bool Type { get; set; }
        public DateTime Date { get; set; }
        public float Price { get; set; }
        public int Volume { get; set; }
        public string Thesis { get; set; }
        public string Description { get; set; }
        public Nullable<int> Balance { get; set; }
        public Nullable<bool> Inend { get; set; }
        public float Averageprice { get; set; }
    }
}
