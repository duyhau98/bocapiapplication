﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Models {
    [Table("bindsymbolwatchlist")]
    public class BindSymbolWatchlist {
        [Key]
        public Nullable<int> Id { get; set; }
        public Nullable<int> Watchlist_id { get; set; }
        public Nullable<int> Symbol_id { get; set; }
        public Nullable<int> User_id { get; set; }
        public Nullable<DateTime> Datetime { get; set; }
        public Nullable<bool> Addremove { get; set; }
        public string Description { get; set; }
    }
}
