﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BOCApiApplication.Common
{
    static public class Utilities
    {
        public static string ExecuteStoreProcedure(string storedProdureName, Dictionary<string, object> parameters)
        {
            try
            {
                using (var connection = new MySqlConnection(Global.ConnectionStrings))
                {
                    connection.Open();
                    using (var cmd = new MySqlCommand(storedProdureName, connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach(var item in parameters)
                        {
                            cmd.Parameters.AddWithValue(item.Key, item.Value);
                        }
                        var reader = cmd.ExecuteReader();
                        var result = Serialize(reader);
                        return JsonConvert.SerializeObject(result);
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public static string GetViewFromDB(string viewName)
        {
            try
            {
                using (var connection = new MySqlConnection(Global.ConnectionStrings))
                {
                    connection.Open();
                    MySqlCommand cmd = connection.CreateCommand();
                    string cmdStr = "SELECT * FROM `"+ viewName + "`" ;
                    cmd.CommandText = cmdStr;
                    var reader = cmd.ExecuteReader();
                    var result = Serialize(reader);
                    return JsonConvert.SerializeObject(result);

                }
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public static string ExecuteQuery(string query)
        {
            try
            {
                using (var connection = new MySqlConnection(Global.ConnectionStrings))
                {
                    connection.Open();
                    MySqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = query;

                    var reader = cmd.ExecuteReader();
                    var result = Serialize(reader);
                    return JsonConvert.SerializeObject(result);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static IEnumerable<Dictionary<string, object>> Serialize(MySqlDataReader reader)
        {
            var results = new List<Dictionary<string, object>>();
            var cols = new List<string>();
            for (var i = 0; i<reader.FieldCount; i++)
                cols.Add(reader.GetName(i));

            while (reader.Read())
                results.Add(SerializeRow(cols, reader));

            return results;
        }

        private static Dictionary<string, object> SerializeRow(IEnumerable<string> cols, MySqlDataReader reader)
        {
            var result = new Dictionary<string, object>();
            foreach (var col in cols)
                result.Add(col, reader[col]);
            return result;
        }
   }
}